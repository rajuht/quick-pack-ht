"use strict";

var tempdir = './',
    gulp = require('gulp'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    prettify = require('gulp-prettify'),
    htmlmin = require('gulp-htmlmin'),
    autoprefixer = require('gulp-autoprefixer'),
    beautify = require('gulp-beautify-code'),
    cleanCss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    include = require('gulp-file-include'),
    notify = require('gulp-notify'),
    w3cjs = require('gulp-w3cjs'),
    through2 = require('through2'),
    imagemin = require('gulp-imagemin'),
    imageminUPNG = require("imagemin-upng"),
    mozjpeg = require('imagemin-mozjpeg'),
    jpegRecompress = require('imagemin-jpeg-recompress'),
    svgo = require('imagemin-svgo'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    plumber = require('gulp-plumber');

var autoprefixerBrowsers = [
    "last 4 version",
    "> 1%",
    "ie >= 9",
    "ie_mob >= 10",
    "ff >= 30",
    "chrome >= 34",
    "safari >= 7",
    "opera >= 23",
    "ios >= 7",
    "android >= 4",
    "bb >= 10"
];


/*--------------------------------------
    Gulp Custom Notifier
----------------------------------------*/
function customPlumber(errTitle) {
    return plumber({
        errorHandler: notify.onError({
            title: errTitle || "Error running Gulp",
            message: "Error: <%= error.message %>",
            sound: "Glass"
        })
    });
}


/*---------------------------
    Gulp Clean App Folder
----------------------------*/
gulp.task('ca', function cleanApp(done) {
    gulp.src('./apps')
        .pipe(customPlumber('Error On Clean App'))
        .pipe(clean());
    done();
});


/*------------------------------
    Gulp Html Validator Check
-------------------------------*/
gulp.task('validator', function htmlValidator(done) {
    gulp.src('./apps/*.html')
        .pipe(w3cjs())
        .pipe(through2.obj(function (file, enc, cb) {
            cb(null, file);
            if (!file.w3cjs.success) {
                throw new Error('HTML validation error(s) found');
            }
        }));
    done();
});

/*------------------------------
    Gulp Html Beautify
-------------------------------*/
gulp.task('htmlBeautify', function htmlBeautify(done) {
    gulp.src('./apps/*.html')
        .pipe(beautify())
        .pipe(gulp.dest('./apps/'));
    done();
});


/*--------------------------------------
    Gulp Image Copy from source to App
----------------------------------------*/
function imgCopy(done) {
    gulp.src('src/img/**/*')
        .pipe(customPlumber('Error On Copy Images'))
        .pipe(gulp.dest('apps/assets/img'));
    done();
}


/*--------------------------------------
    Gulp Fonts Copy from source to App
----------------------------------------*/
function fontsCopy(done) {
    gulp.src('src/fonts/**/*')
        .pipe(customPlumber('Error On Copy Fonts'))
        .pipe(gulp.dest('apps/assets/fonts'));
    done();
}

/*--------------------------------------
    Gulp SCSS Copy from source to App
----------------------------------------*/
function scssCopy(done) {
    gulp.src('src/scss/**/*')
        .pipe(customPlumber('Error On Copy SCSS Files'))
        .pipe(gulp.dest('apps/assets/scss/'));
    done();
}


/*--------------------------------------
    Gulp Revslider Resource Copy
----------------------------------------*/
function revSlider(done) {
    gulp.src('src/js/revslider/**/*')
        .pipe(customPlumber('Error On Copy RevSlider'))
        .pipe(gulp.dest('apps/assets/js/revslider'));
    done();
}


/*--------------------------------------
    Gulp Php Resource Copy
----------------------------------------*/
function phpCopy(done) {
    gulp.src('src/php/**/*')
        .pipe(customPlumber('Error On Copy Php'))
        .pipe(gulp.dest('apps/assets/php'));
    done();
}


/*--------------------------------------
    Gulp CSS Resource Copy
----------------------------------------*/
function cssCopy(done) {
    gulp.src('src/css/**/*')
        .pipe(customPlumber('Error On Copy CSS'))
        .pipe(gulp.dest('apps/assets/css'));
    done();
}


/*------------------------------------------
    Gulp Compile Scss to Css Task & Minify
-------------------------------------------*/
function css(done) {
    var scssFiles = [
        'src/scss/style.scss'
    ];
    gulp.src(scssFiles)
        .pipe(customPlumber('Error On Compile SCSS'))
        .pipe(sass()).on('error', sass.logError)
        .pipe(autoprefixer(autoprefixerBrowsers))
        .pipe(beautify())
        .pipe(gulp.dest('apps/assets/css'))
        .pipe(cleanCss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('apps/assets/css'));
    done();
}


/*--------------------------------------
    Gulp Compile Helper Scss
----------------------------------------*/
gulp.task("helperCss", function helperCss(done) {
    gulp.src(['src/scss/utilities/helper.scss'])
        .pipe(customPlumber('Error On Helper Compile'))
        .pipe(sass()).on('error', sass.logError)
        .pipe(autoprefixer(autoprefixerBrowsers))
        .pipe(cleanCss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('src/css'));
    done();
});

/*--------------------------------------
    Gulp JS Resource Copy
----------------------------------------*/
function jsCopy(done) {
    var jsSrc = [
        'src/js/*.js',
        'src/js/vendor/*.js',
        'src/js/vendor/plugins/*.js'
    ];
    gulp.src(jsSrc)
        .pipe(customPlumber('Error On Copy JS'))
        .pipe(gulp.dest('apps/assets/js'));
    done();
}


/*--------------------------------------
    Gulp Html Compile Task
----------------------------------------*/
function cleanHtml(done) {
    gulp.src('apps/*.html')
        .pipe(customPlumber('Error On clean Html'))
        .pipe(clean());
    done();
}

function compileHtml(done) {
    gulp.src('src/html/*.html')
        .pipe(customPlumber('Error On Compile Html'))
        .pipe(include({
            prefix: '##',
            basepath: 'src/html/partial/'
        }))
        .pipe(beautify())
        .pipe(gulp.dest('apps'));
    done();
}

gulp.task('html', gulp.series(cleanHtml, compileHtml));


/*--------------------------------
    Gulp Liver Server Task
----------------------------------*/
function liveserver(done) {
    browserSync.init({
        server: {
            baseDir: tempdir + 'apps/'
        }
    });
    done();
}

/*--------------------------------
    Gulp Auto Reload Task
----------------------------------*/
function reload(done) {
    browserSync.reload();
    done();
}

/*--------------------------------
    Image Optimization
----------------------------------*/
gulp.task('imgMin', function imgMinify(done) {
    gulp.src('./apps/assets/img/**/*')
        .pipe(imagemin(
            [imageminUPNG(), mozjpeg(), jpegRecompress(), svgo()],
            {verbose: true}
        ))
        .pipe(gulp.dest('./apps/assets/img/'));
    done();
});


/*--------------------------------
    Gulp Files Watch Task
----------------------------------*/
function watchFiles() {
    gulp.watch(['src/**/*.html', 'src/**/*.htm'], gulp.parallel('html', reload));
    gulp.watch('src/scss/**/*', gulp.series(css, scssCopy, reload));
    gulp.watch('src/scss/utilities/helper.scss', gulp.series('helperCss', reload));
    gulp.watch('src/img/**/*', gulp.series(imgCopy, reload));
    gulp.watch('src/css/**/*', gulp.series(cssCopy, reload));
    gulp.watch('src/js/**/*', gulp.series(jsCopy, reload));
    gulp.watch('src/js/revslider/**/*.js', gulp.series(revSlider, reload));
    gulp.watch('src/fonts/**/*', gulp.series(fontsCopy, reload));
    gulp.watch('src/php/**/*', gulp.series(phpCopy, reload));
}


// Gulp All Files Copy Task to App
gulp.task('filesCopy', gulp.parallel(imgCopy, fontsCopy, css, revSlider, phpCopy, scssCopy, 'helperCss', 'html'));


// Gulp CSS, JS Vendor & Minify Task
gulp.task('vendor', gulp.series(cssCopy, jsCopy));


// Gulp Default Task Run On Terminal
gulp.task('default', gulp.series('filesCopy', 'vendor', gulp.parallel(liveserver, watchFiles)));


/*================================
Dist Preview Files Make Scripts
=========================================*/

/*--- Landing Page Files Copy ----*/
gulp.task('landingFilesCopy', function (done) {
    gulp.src('landing/apps/**/*')
        .pipe(gulp.dest('dist/arden-preview/'));
    done();
});

function previewImgcopy(done) {
    gulp.src('apps/assets/img/**/*')
        .pipe(imagemin(
            [imageminUPNG(), mozjpeg(), jpegRecompress(), svgo()],
            {verbose: true}
        ))
        .pipe(gulp.dest('dist/arden-preview/arden-demo/assets/img'));
    done();
}

function previewPhpCopy(done) {
    gulp.src('apps/assets/php/**/*')
        .pipe(gulp.dest('dist/arden-preview/arden-demo/assets/php'));
    done();
}

function previewFontsCopy(done) {
    gulp.src('apps/assets/fonts/**/*')
        .pipe(gulp.dest('dist/arden-preview/arden-demo/assets/fonts'));
    done();
}

gulp.task('previewFilesCopy', gulp.series(previewImgcopy, previewPhpCopy, previewFontsCopy));

gulp.task('mainHtmlCopy', function (done) {
    gulp.src('apps/*.html')
        .pipe(useref())
        .pipe(gulpif('*.html', htmlmin({collapseWhitespace: true})))
        .pipe(gulp.dest('dist/arden-preview/arden-demo'));
    done();
});

gulp.task('dist-preview', gulp.series('landingFilesCopy', 'previewFilesCopy', 'mainHtmlCopy'));


/*====================================
Dist Downloadable Files Make Scripts
============================================*/
function downloadImgcopy(done) {
    gulp.src('apps/assets/img/**/*')
        .pipe(imagemin(
            [imageminUPNG(), mozjpeg(), jpegRecompress(), svgo()],
            {verbose: true}
        ))
        .pipe(gulp.dest('dist/downloadable-files/arden/assets/img'));
    done();
}

function downloadPhpCopy(done) {
    gulp.src('apps/assets/php/**/*')
        .pipe(gulp.dest('dist/downloadable-files/arden/assets/php'));
    done();
}

function downloadFontsCopy(done) {
    gulp.src('apps/assets/fonts/**/*')
        .pipe(gulp.dest('dist/downloadable-files/arden/assets/fonts'));
    done();
}

function downloadscssCopy(done) {
    gulp.src('apps/assets/scss/**/*')
        .pipe(gulp.dest('dist/downloadable-files/arden/assets/scss'));
    done();
}

function downloadCssCopy(done) {
    gulp.src('apps/assets/css/**/*')
        .pipe(beautify())
        .pipe(gulp.dest('dist/downloadable-files/arden/assets/css'));
    done();
}

function downloadJsCopy(done) {
    gulp.src('apps/assets/js/**/*')
        .pipe(beautify())
        .pipe(gulp.dest('dist/downloadable-files/arden/assets/js'));
    done();
}

function downloadHtmlCopy(done) {
    gulp.src('apps/*.html')
        .pipe(beautify())
        //.pipe(prettify())
        .pipe(gulp.dest('dist/downloadable-files/arden/'));
    done();
}

function docsCopy(done) {
    gulp.src('./documentation/**/*')
        .pipe(beautify())
        //.pipe(prettify())
        .pipe(gulp.dest('dist/downloadable-files/documentation'));
    done();
}

gulp.task('dist-file', gulp.series(downloadImgcopy, downloadPhpCopy, downloadFontsCopy, downloadscssCopy, downloadJsCopy, downloadCssCopy, downloadHtmlCopy, docsCopy));