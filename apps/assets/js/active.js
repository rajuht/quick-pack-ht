/*global jQuery */
(function ($) {
    "use strict";

    /*===============================
        ----- JS Index -----

    01. Background Image JS
    ==================================*/

    jQuery(document).ready(function ($) {

        /*--------------------------
            01. Background Image JS
        ---------------------------*/
        var bgSelector = $("[data-bg]");
        bgSelector.each(function (index, elem) {
            var element = $(elem),
                bgSource = element.data('bg');
            element.css('background-image', 'url(' + bgSource + ')');
        });
    }); //End Ready Function

    jQuery(window).on('scroll', function () {
        
    }); // End Scroll Function

    jQuery(window).on('load', function () {
       
    }); // End Load Function
}(jQuery));